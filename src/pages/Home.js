
import Banner from './../components/Banner'
import Highlights from './../components/Highlights'


//lets create a databa object that will describe the content of the hero section
const data = {
    title: 'Welcome to the Home Page',
    content: 'Oppurtunities for everyone,everywhere'
}

export default  function Home(){
    return (
        <div>
         
        <Banner bannerData={data}/>
        <Highlights/>
        </div>
    );
};
