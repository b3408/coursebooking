import AppSideNavBar  from '../components/AppSideNavBar';
import Banner from '../components/Banner'
import Highlights from '../components/Highlights'


//lets create a databa object that will describe the content of the hero section
const data = {
    title: 'Welcome to the Admin Page',
    content: 'Oppurtunities for everyone,everywhere'
}

export default  function Home(){
    return (
        <div>
        <AppSideNavBar/>
        <div className='mr-5'>
        <Banner bannerData={data}/>
        </div>
        <Highlights/>
        </div>
    );
};
