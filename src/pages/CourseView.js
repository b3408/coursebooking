//this will serve as page wheneever the client would want to select a single course/item from catalog
import Banner from './../components/Banner';

//Grid System
import {Row, Col, Card, Button,Container} from 'react-bootstrap'
//routing component
import { Link, useParams } from 'react-router-dom';
import swal from 'sweetalert2'
import { useState, useEffect } from 'react';




const data = {
    title: 'Welcome to b156 Booking-App',
    content:  'Check out our school Campus'
};

export default function CourseView(){
    //State of our course details
    const [courseInfo, setCourseInfo] = useState({
        name: null,
        description: null,
        price: null
    });

    console.log(useParams());
    const {id} = useParams()
    console.log(id);

    useEffect(() =>{
        fetch(`https://lit-beach-97738.herokuapp.com/courses/${id}`)
        .then(res => res.json())
        .then(convertedData => {
            setCourseInfo({
                name: convertedData.name,
                description: convertedData.description,
                price: convertedData.price
            })
        });
    },[id])


    const enroll = () =>{
        return(
            swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Enrolled Successfully!',
                text: 'Thank you for enrolling to this course',
                showConfirmButton: false,
                timer: 1500
              }) 
        );
    };
    return(
        <div>
          
            <Banner bannerData={data}/>
            <Row>
                {/* COURSE 1 HIGHLIGHT */}
                <Col>
                <Container>
                    <Card className='text-center'>
                       <Card.Body>
                           {/* Course Name */}
                        <Card.Title>
                            <h2>{courseInfo.name}</h2>
                        </Card.Title>
                        {/* Course Description */}
                        <Card.Subtitle>
                            <h6 className='my-4'> Description</h6>
                        </Card.Subtitle>
                         <Card.Text>
                         {courseInfo.description}
                         </Card.Text>
                         <Card.Subtitle>
                            <h6 className='my-4'> Price </h6>
                         </Card.Subtitle>
                         <Card.Text>
                            {courseInfo.price}
                         </Card.Text>
                        </Card.Body> 
                      <Button className='bg-warning btn-block' onClick={enroll}>
                      Enroll
                      </Button>
                      <Link className='btn btn-success btn-block'to="/login">
                          Login to Enroll
                      </Link>
                    </Card>
                </Container>    
                </Col>
                
            </Row>
        </div>
    )
};