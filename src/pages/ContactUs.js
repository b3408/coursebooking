//idnentify the components needed to create the register page

import { Container, Form, Button } from 'react-bootstrap';
import swal from 'sweetalert2'


export default function Register(){
 
    const contactUs = (eventSubmit) => {
        eventSubmit.preventDefault()
        
        return(
                document.getElementById('contactUsForm').reset(),
                swal.fire({
                icon: 'success',
                title: 'Message Sent Successfully!',
                text: 'Thank you for reaching out!',
              }) 
            
        );
    };

    return(
        <div>
    
            <Container>
           <h1 className='text-center mt-5'>Contact Form</h1>
           <Form onSubmit={e => contactUs(e)} id='contactUsForm'>
                {/* First Name Field */}
               <Form.Group>
                   <Form.Label>Full Name</Form.Label>
                   <Form.Control type='text' placeholder='Enter your Full Name' required></Form.Control>
               </Form.Group>
                {/* Emaill address Field */}
               <Form.Group>
                   <Form.Label>Email</Form.Label>
                   <Form.Control type='email'placeholder='Enter your email' required></Form.Control>
               </Form.Group>
                {/* Message */}
               <Form.Group>
                   <Form.Label>Message</Form.Label>
                   <Form.Control type='text' placeholder='Enter your message ' required></Form.Control>
               </Form.Group>       
                  {/* Send Button */}
                  <Button className='btn-block' type='submit'>Send</Button>     
           </Form>
        
       </Container>
        </div>
        
    );
};