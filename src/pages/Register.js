//idnentify the components needed to create the register page
import {useState, useEffect, useContext} from 'react'
import UserContext from '../UserContext';
import Banner from './../components/Banner'
import { Container, Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import swal from 'sweetalert2'

const data = {
    title: 'Welcome to the Register Page',
    content: 'Create an Account to Enroll'
}



export default function Register(){
    const {user} = useContext(UserContext);
    const  [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');

    const [isActive, setIsActive] = useState(false);
    const [isMatched, setIsMatched] = useState(false);
    const [isMobileValid, setIsMobileValid] = useState(false)
    const [isAllowed, setIsAllowed] = useState(false)

    useEffect(() => {
        if (mobileNo.length === 11 ) {
            if (
                password1 === password2 && 
                password1 !== '' && password2 !== ''
            ) {
                setIsMatched(true);
                if (firstName !== '' && lastName !== '' && email !== '' )  {
                    setIsActive(true)
                    setIsAllowed(true)
                } else {
                    setIsActive(false)
                    setIsAllowed(false)
                }
            } else {
                setIsMatched(false)
            }
             setIsMobileValid(true)
            
        }else if(password1 !== '' && password1 === password2){
            setIsMatched(true)
        }else {   
            setIsMobileValid(false)
            setIsMatched(false)
            setIsAllowed(false)
            setIsActive(false)   
            }
    },[firstName, lastName, email, password1, password2, mobileNo]);

    const registerUser = async (eventSubmit) => {
        eventSubmit.preventDefault()

        const isRegistered = await fetch('https://lit-beach-97738.herokuapp.com/users/register/', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password1, 
				mobileNo: mobileNo
			})
		}).then(response => response.json())
		.then(dataNakaJSON => {
			//console.log(dataNakaJSON); //user object
			//create a control structure if success or fail. 
			//check if the email props has data.
            if (dataNakaJSON.email) {
                return true
            } else {
                return false
            }
		})
        if (isRegistered) {
            setFirstName('');
            setLastName('');            
            setEmail('');
            setMobileNo('');
            setPassword1('');
            setPassword2('');

            await swal.fire({
                icon: 'success',
                title: 'Registered Successfully!',
                text: 'Please wait while redirecting.',
              })  
         window.location.href = "/login";  
        } else {
            await swal.fire({
                icon: 'error',
                title: 'Something Went Wrong',
                text: 'Try Again Later',
              }) 
        }
     };
    return(
       user.id ?
       <Navigate to='/courses'replace={true}/> 
       :
        <div>
        
        <Banner bannerData={data}/>
            <Container>
                {   
                    isAllowed ?    
                    <h1 className='text-center text-success'>You May Now Register!</h1>
                    :  
                    <h1 className='text-center'>Register Form</h1>
                }
           <h6 className='text-center mt-3 text-secondary'>Fill Up the Form Below</h6>
           <Form onSubmit={e => registerUser(e)} id='registerForm'>
                {/* First Name Field */}
               <Form.Group>
                   <Form.Label>First Name</Form.Label>
                   <Form.Control 
                   type='text' 
                   placeholder='Enter your first Name' 
                   required value={firstName} 
                   onChange={event => {setFirstName(event.target.value)}}>

                   </Form.Control>
               </Form.Group>
                {/* Last Name Field */}
               <Form.Group>
                   <Form.Label>Last Name</Form.Label>
                   <Form.Control type='text'placeholder='Enter your last Name' required value={lastName} onChange={event => {setLastName(event.target.value)}}></Form.Control>
               </Form.Group>
                {/* Emaill address Field */}
               <Form.Group>
                   <Form.Label>Email</Form.Label>
                   <Form.Control type='email'placeholder='Enter your email' required value={email} onChange={event => {setEmail(event.target.value)}}></Form.Control>
               </Form.Group>
                {/* Mobile Field */}
               <Form.Group>
                   <Form.Label>Mobile Number</Form.Label>
                   <Form.Control type='tel' pattern='[0]{1}[8-9]{1}[0-9]{9}' placeholder='Ex.09975110865' required value={mobileNo} onChange={event => {setMobileNo(event.target.value)}}></Form.Control>
                   {
                       isMobileValid?
                       <span className='text-success'>Mobile is Valid </span>
                       :
                       <span className='text-secondary'>Mobile No. 11 digits </span>
                   }
               </Form.Group>   
                {/* Password Field */}
               <Form.Group>
                   <Form.Label>Password</Form.Label>
                   <Form.Control type='password' placeholder='Enter your  password' required value={password1} pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$" title="Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character" onChange={event => {setPassword1(event.target.value)}}></Form.Control>
               </Form.Group>
                {/* Confirm Password Field */}
               <Form.Group>
                   <Form.Label>Confirm Password</Form.Label>
                   <Form.Control type='password' placeholder='Confirm your  password'   required value={password2} pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$" title="Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character"  onChange={event => {setPassword2(event.target.value)}}>
                   </Form.Control>
                   {
                       isMatched ?
                       <span className='text-success'>Password Matched </span>
                       :
                       <span className='text-danger'>Password don't match</span>
                   }
               </Form.Group>
                {/* Register Button */}
                
                {
                 isActive ?
                    <Button className='btn-block' type='submit'>Register</Button>
                      :
                    <Button className='btn-block' disabled>Register</Button>
                }
                
           </Form>
        
       </Container>
        </div>
        
    );
};