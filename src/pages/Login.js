import {useEffect, useState, useContext} from 'react'
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import {Form, Button, Container} from 'react-bootstrap'
import Swal from 'sweetalert2'
import { Link } from 'react-router-dom';


export default function Login() {
    
    //Declare an 'initial'/default state for our form elements. 
    //Bind/Lock the form elements to the desired states
    //Assign the states to their respective components
    //SYNTAX: const/let [getter, setter] = useState()
    const [email, setEmail ] = useState('');	
    const [password, setPassword ] = useState(''); 
    const {user, setUser} = useContext(UserContext)
    //what validation are we going run on the email. 
    //(format: @ , dns)
    //search a character within a string
    let addressSign = email.search('@'); 
    //if the search() finds No match inside the string it will return -1. 
    //tip: if youre going to pass down multiple values on the query. contain inside an array. includes()
    let dns = email.search('.com');

    //declare a state for the login button
    const [isActive, setIsActive] = useState(false);
    //apply a conditional rendering to the button component for its current state. 
    const [isValid, setIsValid] = useState(false);

    //create a side effect that will make our page 'reactive'.
    useEffect(() => {
		//Create a logic/condition that will evaluate the format of the email.
		//if the value is -1 (no match found inside the string)
    	if (dns !== -1 && addressSign !== -1 ) {
    		setIsValid(true);
    		if (password !== '') {
    			setIsActive(true);
    		} else {
    			setIsActive(false);
    		}
    	} else {
    		setIsValid(false); 
    		setIsActive(false); 
    	}
    },[email, password, addressSign, dns]) 

	//Create a function that will run the request for user authentication. to produce an acccess token. 
	const loginUser = async (event) => {
		event.preventDefault(); 
		
		//send a request to verify if the user's identity is true.
		//syntax: fetch('url', {options}) 
		fetch('https://lit-beach-97738.herokuapp.com/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}).then(res => res.json())
		.then(dataNaJson => {
			//console.log(dataNaJson); //token 
			let token = dataNaJson.access; 
			//console.log(token); //who ?

			//create a control structure to give a proper response to the user 
			if (typeof token !== 'undefined') {
				//prompt ng message kay user

				//The produced token, save it on the browser storage. (storage area)
				//to save an item in the localStorage object of the browser... use setItem(key, value)
				                    //key, value
				localStorage.setItem('accessToken',token);
                fetch('https://lit-beach-97738.herokuapp.com/users/details',{
                    headers:{
                      Authorization: `Bearer ${token}`
                    }
                  })
                  .then(res => res.json())
                  .then(convertedData =>{
                    if (typeof convertedData._id !== "undefined") {
                      setUser({
                        id: convertedData._id,
                        isAdmin: convertedData.isAdmin
                      });
                       Swal.fire({
                          icon: 'success',
                          title: 'Login Successful',
                          text: 'Welcome!'
                      })	
                    }else {
                      setUser({
                        id: null,
                        isAdmin: null
                      })
                    };
                  });
                window.location.href = "/";  
			} else {
				Swal.fire({
					icon: 'error',
					title: 'Check your Credentials',
					text: 'Contact Admin if problem persist'
				})	
			}
		})

		
	};

    return(
        user.id ? 
        <Navigate to='/courses' replace={true}/>
        :

        <div> 
            <Container className='mt-5'>
                 <h1 className='text-center'>Login Form</h1>
                <div className='border container mt-5' id='formBox'>
                 <Form onSubmit={event => loginUser(event)} id='formLogin'>
                     {/* Email Address Field */}
                     <Form.Group className='mt-5'>
                         <Form.Label>Email</Form.Label>
                         <Form.Control type='email' required placeholder='Enter your email' value={email} onChange={event => {setEmail(event.target.value)}}></Form.Control>
                         {
                             isValid ? 
                             <h6 className='text-success'>Email is Valid</h6>
                             :
                             <h6 className='text-mute'>Email is Invalid</h6>
                         }
                     </Form.Group>
                     {/* Password Field */}
                     <Form.Group className='mt-2'>
                         <Form.Label>Password</Form.Label>
                         <Form.Control type='password' required placeholder='Enter your password' value={password} onChange={event => {setPassword(event.target.value)}}></Form.Control>
                     </Form.Group>
                     {
                        isActive ?
                        <Button className='btn btn-block ' type='submit'>Login</Button>
                         :
                         <Button className='btn btn-block btn-secondary' type='submit' disabled>Login</Button>
                     }
                 </Form>
                 <p className='mt-3'>Not registered yet?<Link to="/register">
                          Click to Register
                      </Link></p>
                 </div>
            </Container>
        </div>
    );
}