import { useEffect, useState } from 'react';

import Banner from './../components/Banner';
import CourseCard from './../components/CourseCard';
import { Container } from 'react-bootstrap';



const data = {
    title:'Course Catalog',
    content:'Browse through our Catalog of courses'
}



export default function Courses(){
    const [courseCollection, setCourseCollection] = useState([]);
    useEffect(()=> {
        fetch('https://lit-beach-97738.herokuapp.com/courses/').then(res => res.json()).then(convertedData => {
            setCourseCollection(convertedData.map(course => {
                return(
                    <CourseCard key={course._id}  courseProp={course} />
                )
            })) 
        });
    },[]);
   
    return(
        <div>
        
        <Banner bannerData={data}/>
        <Container>
           {courseCollection}
        </Container>
        
        
        </div>
    );
}
