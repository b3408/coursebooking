//identify which componts are needed to build the navigation
import { useContext } from 'react';
import { Navbar, Nav, Container, NavDropdown} from 'react-bootstrap'
//implent links in the navbar
import { Link } from 'react-router-dom';
//we will now describe jpw we want pir Navbar to look.
import { faUserEdit } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import UserContext from '../UserContext';

function AppNavBar(){

     const { user } = useContext(UserContext) ;
    
    return(
    <Navbar bg="primary" expand="lg">
     <Container>
        <Navbar.Brand>B156 Booking App</Navbar.Brand>
        <Navbar.Toggle aria-label="basic-navbar-nav"/>
        <Navbar.Collapse>
            <Nav className='ml-auto'>
               <Link to="/" className='nav-link'>Home</Link>
               <Link to="/courses" className='nav-link'>Courses</Link>
               {user.id !== null ? 
                 <NavDropdown  title={
                    <FontAwesomeIcon icon={faUserEdit}></FontAwesomeIcon>
                            } id="nav-dropdown">
                    <NavDropdown.Item eventKey="4.1">My Cart</NavDropdown.Item>
                    <NavDropdown.Item eventKey="4.2">Change Password</NavDropdown.Item>
                    <NavDropdown.Item eventKey="4.3">Settings</NavDropdown.Item>
                    <NavDropdown.Item href="/logout">Logout</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item eventKey="4.4">Need Help?</NavDropdown.Item>
                </NavDropdown> 
               :
               <>
                <Link to="/register" className='nav-link'>Register</Link>   
                <Link to="/login" className='nav-link'>Login</Link> 
               </>
               }
            </Nav>   
        </Navbar.Collapse>
     </Container>

    </Navbar>
        );
    };
    export default AppNavBar;
    
    