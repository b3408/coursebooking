//this component will b e used as the heroe section of our page.

//responsivee
import {Row, Col} from 'react-bootstrap';
//we will use default bootstrap utility classes to format the component
//create the fucntion that will describe the structure of the hero section
//class > reserve keyword
//react jsx className

export default function Banner({bannerData}) {
    return(
        <Row className='p-5'>
            <Col className='text-center'>
            <h1> {bannerData.title}</h1>
                <p className='my-3'>{bannerData.content}</p>
                <a className='btn btn-primary' href='/'>INSERT ACTION HERE</a>  
            </Col>
          
        </Row>
    );
};

