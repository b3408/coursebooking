//identify needed components
import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';



export default function courseCard({courseProp}){
  return(
      <Card className='m-4 d-md-inline-flex d-sm-inline-flex cardForm'>
          <Card.Body> 
              <Card.Title>
                 {courseProp.name}
              </Card.Title>
              <Card.Text>{courseProp.description}</Card.Text>
              <Card.Text> Price: {courseProp.price} </Card.Text>
              <Link to={`view-course/${courseProp._id}`}  className='btn btn-primary'>View course</Link>
          </Card.Body>
      </Card>
  )  
};