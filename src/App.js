import './App.css';
import { useState, useEffect } from 'react';
// import AppSideNavBar from './components/AppSideNavBar';
// import AppNavBar from './components/AppNavBar.js';
import Home from './pages/Home.js';
import Register from './pages/Register.js';
import Courses from './pages/Courses.js';
import Error from './pages/Error.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import CourseView from './pages/CourseView';
import AddCourse from './pages/AddCourse';
// import UpdateCourse from './pages/UpdateCourse';
// import AdminHome from './pages/AdminHome';
import ContactUs from './pages/ContactUs';
import {UserProvider} from './UserContext'
//implement page routing in our app
//aquire the utilities from react router dom
import {BrowserRouter as Router, Routes, Route}from 'react-router-dom'
import AppNavBar from './components/AppNavBar';
//BrowserRouter - this is a standard library component for routing in react
//this enables navigation amongst views of various componentsit will servee as the parent component that will used to store all the eother compomnents
//Routes=> it's a new component introduced in v6 of react-router-dom whose task is to allow switching betweeen locations


//JSX COMPONENT SELF CLOSING TAg
function App() {
  const [user, setUser] = useState({
    id:null,
    isAdmin: null
  }); 

  const unsetUser = () => {
    localStorage.clear();
    setUser({
      id: null,
      isAdmin: null
    })
  }


  useEffect(() => {
    let token = localStorage.getItem('accessToken');
    fetch('https://lit-beach-97738.herokuapp.com/users/details',{
      headers:{
        Authorization: `Bearer ${token}`
      }
    })
    .then(res => res.json())
    .then(convertedData =>{
      if (typeof convertedData._id !== "undefined") {
        setUser({
          id: convertedData._id,
          isAdmin: convertedData.isAdmin
        })
      }else {
        setUser({
          id: null,
          isAdmin: null
        })
      };
    });
  },[user]);

  return (
    <div>
      <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
       <AppNavBar/>
          <Routes>
              <Route path='/' element={<Home/>}/>
              <Route path='/register' element={<Register/>} />
              <Route path='/login' element={<Login/>}/>
              <Route path='/courses' element={<Courses/>}/>   
              <Route path='/courses/view-course/:id' element={<CourseView/>}/>
              <Route path='/logout' element={<Logout/>}/>
              <Route path='/courses/add-course' element={<AddCourse/>}/>
              {/* <Route path='/courses/update-course' element={<UpdateCourse/>}/> */}
              <Route path='*' element={<Error/>}/>
              {/* <Route path='/admin' element={<AdminHome/>}/> */}
              <Route path='/contact-us' element={<ContactUs/>}/>
              
          </Routes>
      </Router>
      </UserProvider>
      
    </div>
  );
}

export default App;
